#include "student-b.h"

int isOdd(int digit) // jeśli zwraca 0 to liczba jest parzysta , a jeśli 1 to nieparzysta
{
    if(digit % 2 == 0)
        return 0;
    else
        return 1;
}

int min(int digits[], int size)
{
    int min = digits[0];
    for(int i=1; i<size; i++)
    {
        if(min > digits[i])
        {
            min = digits[i];
        }
    }
    return min;
}
